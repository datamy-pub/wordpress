#!/usr/bin/env bash

vagrant destroy -f

if [ -d "wordpress" ]
then
    rm -r wordpress
fi

./up.sh
